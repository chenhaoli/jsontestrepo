#pragma once
#include "include.h"

class JsonData
{
private:
	/*
	{
		"name": "Chen Haoli",
		"age": 25,
		"addr": "ZheJiang NingBo",
	}
	*/
	string objName;
	string name;
	int age;
	string addr;

public:
	JsonData(const string objName, const string newName, const int newAge, const string newAddr);


	void setName(const string newName);
	void setAge(const int newAge);
	void setAddr(const string newAddr);

	string getObjName() const;
	string getName() const;
	int getAge() const;
	string getAddr() const;

	void init(const JsonData jsonData);

	void load();
	void calc();
	void save() const;
	void cleanUp();
};

