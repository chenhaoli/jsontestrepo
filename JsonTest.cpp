﻿#include "JsonIOTest.h"

const string fileName = "test1.json";

int main()
{
	JsonIOTest* jIOTest = new JsonIOTest();
	jIOTest->init(fileName);
	jIOTest->load(fileName);
	jIOTest->save();
	jIOTest->calc();
	jIOTest->save();
	jIOTest->cleanUp();
}