#include "JsonIOTest.h"

JsonIOTest::JsonIOTest() {

}
JsonIOTest::~JsonIOTest() {

}

void JsonIOTest::init(const string fileName) {
	ofstream ofs1(fileName, ios::out);
	ofs1 << "[";

	JsonData* jd1 = new JsonData("CHL", "Chen Haoli", 25, "ZheJjiang NingBo");
	JsonData* jd2 = new JsonData("LWY", "Lv Wenya", 25, "ShanDong YanTai");
	ofs1 << "{";
	ofs1 << "\"" << jd1->getObjName() << "\":";
	ofs1 << "{";
	ofs1 << "\"name\":\"" << jd1->getName() << "\",";
	ofs1 << "\"age\":" << jd1->getAge() << ",";
	ofs1 << "\"addr\":\"" << jd1->getAddr() << "\"";
	ofs1 << "}";
	ofs1 << "},";

	ofs1 << "{";
	ofs1 << "\"" << jd2->getObjName() << "\":";
	ofs1 << "{";
	ofs1 << "\"name\":\"" << jd2->getName() << "\",";
	ofs1 << "\"age\":" << jd2->getAge() << ",";
	ofs1 << "\"addr\":\"" << jd2->getAddr() << "\"";
	ofs1 << "}";
	ofs1 << "}";

	ofs1 << "]" << endl;

	ofs1.close();

	jd1->cleanUp();
	jd2->cleanUp();

	cout << "init success!" << endl;
}

void JsonIOTest::load(const string fileName) {
	ifstream ifs(fileName);
	IStreamWrapper isw(ifs);
	Document document;
	document.ParseStream(isw);
	ifs.close();
	for (int i = 0; i < document.Size(); ++i) {
		for (Value::ConstMemberIterator itr = document[i].MemberBegin(); itr != document[i].MemberEnd(); ++itr) {
			const Value& v = itr->value;
			string objName = itr->name.GetString();
			vecJsonData.push_back(
				new JsonData(objName, v["name"].GetString(), v["age"].GetInt(), v["addr"].GetString())
			);
		}
	}
	
	cout << "load success!" << endl;
}
void JsonIOTest::calc() {
	for (auto itr = vecJsonData.begin(); itr != vecJsonData.end(); ++itr) {
		(*itr)->setAge((*itr)->getAge() + 1);
		cout << "objName:" << (*itr)->getObjName() << "-->";
		cout << "age++ success!" << endl;
	}
}
void JsonIOTest::save() const{
	for (auto itr = vecJsonData.begin(); itr != vecJsonData.end(); ++itr) {
		cout << "objName:" << (*itr)->getObjName() << "-->";
		cout << "name:" << (*itr)->getName() << "; ";
		cout << "age:" << (*itr)->getAge() << "; ";
		cout << "addr:" << (*itr)->getAddr() << endl;
	}
}
void JsonIOTest::cleanUp() {
	cout << "clean up JsonIOTest obj!" << endl;
	delete this;
}