#pragma once
#include "JsonData.h"

class JsonIOTest
{
private:
	vector<JsonData*> vecJsonData;

public:
	JsonIOTest();
	~JsonIOTest();

	void init(const string fileName);
	void load(const string fileName);
	void calc();
	void save() const;
	void cleanUp();
};

