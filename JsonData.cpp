#include "JsonData.h"

JsonData::JsonData(const string newObjName, const string newName, const int newAge, const string newAddr) {
	objName = newObjName;
	name = newName;
	age = newAge;
	addr = newAddr;
}

string JsonData::getObjName() const {
	return objName;
}

string JsonData::getName() const{
	return name;
}

int JsonData::getAge() const {
	return age;
}

string JsonData::getAddr() const {
	return addr;
}

void JsonData::setName(const string newName) {
	name = newName;
}

void JsonData::setAge(const int newAge) {
	age = newAge;
}

void JsonData::setAddr(const string newAddr) {
	addr = newAddr;
}

void JsonData::init(const JsonData jsonData) {
	objName = jsonData.objName;
	name = jsonData.name;
	age = jsonData.age;
	addr = jsonData.addr;
}

void JsonData::load() {

}

void JsonData::calc() {

}

void JsonData::save() const{
	cout << "name:" << name << endl;
	cout << "age:" << age << endl;
	cout << "addr:" << addr << endl;
}

void JsonData::cleanUp() {
	cout << "clean up JsonData obj!" << endl;
	delete this;
}